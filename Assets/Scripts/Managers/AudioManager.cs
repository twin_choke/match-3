﻿using UnityEngine;

public enum SoundEffects
{
    Selected,
    Swapped,
    Cleared
}

public class AudioManager : MonoBehaviour
{
    public static AudioManager instance;

    [SerializeField] AudioSource playerSfxSource;
    [SerializeField] AudioSource comboSfxSource;

    [SerializeField] AudioClip selectSound;
    [SerializeField] AudioClip swapSound;
    [SerializeField] AudioClip clearSound;
    [SerializeField] AudioClip comboSound;

    private void Awake()
    {
        if (instance == null)
            instance = this;

        else if (instance != this)
            Destroy(gameObject);
    }

    private void Start()
    {
        comboSfxSource.clip = comboSound;
    }

    public void PlayPlayersTurnEffect(SoundEffects effect)
    {
        switch (effect)
        {
            case SoundEffects.Selected:
                playerSfxSource.clip = selectSound;
                break;

            case SoundEffects.Swapped:
                playerSfxSource.clip = swapSound;
                break;

            case SoundEffects.Cleared:
                playerSfxSource.clip = clearSound;
                break;
        }

        playerSfxSource.Play();
    }

    public void PlayComboEffect() => comboSfxSource.Play();
}