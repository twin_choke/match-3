﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScoreHandler : MonoBehaviour
{
    public static ScoreHandler instance;
    public event Action<int> UpdateScore;

    [SerializeField] float comboMulti;
    [SerializeField] float multiGrowPerCombo;
    [SerializeField] int emptyTurnPenalty;

    Dictionary<Sprite, int> SpriteRewardDict;

    int totalScore = 0;
    int comboCounter = 0;
    int scoreRecord;
    int comboRecord;

    private void Awake()
    {
        if (instance == null)
            instance = this;
        else if (instance != this)
            Destroy(gameObject.GetComponent<ScoreHandler>());
    }

    public void Init()
    {
        SpriteRewardDict = new Dictionary<Sprite, int>(TileCreate.instance.TileSet.Tiles.Count);

        foreach (var tile in TileCreate.instance.TileSet.Tiles)
        {
            SpriteRewardDict.Add(tile.Sprite, tile.Reward);
        }

        LineClear.instance.LineCleared += HandleScoreUpdate;
        GameManager.instance.EmptyTurn += OnEmptyTurn;
        GameManager.instance.ComboCounterChanged += OnComboCounterUpdate;
        scoreRecord = PlayerPrefs.GetInt("Highscore");
        comboRecord = PlayerPrefs.GetInt("ComboRecord");
    }

    void HandleScoreUpdate(TileData[] line)
    {
        int finalReward;

        int lineRawReward = SpriteRewardDict[line[0].Renderer.sprite] * line.Length;
        if (comboCounter > 1)
            finalReward = Mathf.RoundToInt(lineRawReward * (comboCounter - 1) * (comboMulti + comboMulti * multiGrowPerCombo));

        else finalReward = lineRawReward;

        totalScore += finalReward;
        if (totalScore > scoreRecord)
        {
            comboRecord = totalScore;
            PlayerPrefs.SetInt("Highscore", comboRecord);
        }

        UpdateScore?.Invoke(totalScore);
    }

    void OnComboCounterUpdate(int count)
    {
        if (count > comboRecord)
        {
            comboRecord = count;
            PlayerPrefs.SetInt("ComboRecord", comboRecord);
        }
    } 
    
    void OnEmptyTurn()
    {
        if (totalScore - emptyTurnPenalty > 0)
            totalScore -= emptyTurnPenalty;
        else totalScore = 0;

        UpdateScore?.Invoke(totalScore);
    }
}
