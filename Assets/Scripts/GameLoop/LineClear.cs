﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LineClear : MonoBehaviour
{
    public static LineClear instance;
    public event Action<TileData[]> LineCleared;

    [SerializeField] float particlesMoveSpeed;
    [SerializeField] float particlesTimePerTile;
    [SerializeField] float tileDropAcceleration;

    Dictionary<Sprite, List<ParticleSystem>> ParticlesDict = new Dictionary<Sprite, List<ParticleSystem>>();
    GameObject emptyTileObj;

    CoroutinesObserver clearLinesObserver = new CoroutinesObserver();
    CoroutinesObserver dropDownObserver = new CoroutinesObserver();
    CoroutinesObserver fillEmptyObserver = new CoroutinesObserver();

    private void Awake()
    {
        if (instance == null)
            instance = this;
        else if (instance != this)
            Destroy(gameObject.GetComponent<LineClear>());
    }

    public void Init()
    {
        emptyTileObj = TileCreate.instance.emptyTileObj;
    }

    public IEnumerator RunLinesClearing()
    {
        clearLinesObserver.Reset();
        dropDownObserver.Reset();
        fillEmptyObserver.Reset();

        bool finished = false;
        Action ClearingLinesFinished = () => finished = true;
        StartCoroutine(ClearDirector(ClearingLinesFinished));
        while (!finished)
            yield return null;
    }

    IEnumerator ClearDirector(Action Finished)
    {
        List<TileData[]> LinesList = TileLineSearch.instance.TileLinesList;

        int linesTotal = LinesList.Count;
        int linesCleared = 0;
        Action CoroutineReady = () => linesCleared++;
        
        for (int i = 0; i < linesTotal; i++)
        {
            StartCoroutine(ClearLine(LinesList[i], clearLinesObserver.JobFinished));
            clearLinesObserver.JobAdded();
        }

        while (!clearLinesObserver.AllJobsFinished())
            yield return null;

        clearLinesObserver.Reset();

        TileData[,] boardTiles = TileCreate.instance.boardTilesData;

        for (int x = 0; x < TileCreate.instance.XSize; x++)
        {
            int tileDropOffset = 0;

            for (int y = 0; y < TileCreate.instance.YSize; y++)
            {
                TileData tData = boardTiles[x, y];
                if (tData.tileObj != emptyTileObj)
                {
                    if (tileDropOffset > 0)
                    {
                        var targetTileData = boardTiles[tData.boardX, tData.boardY - tileDropOffset];
                        StartCoroutine(DropTile(tData, targetTileData, dropDownObserver.JobFinished));
                        dropDownObserver.JobAdded();
                    }

                }

                else tileDropOffset += 1;
            }
        }
        
        while (!dropDownObserver.AllJobsFinished())
            yield return null;

        dropDownObserver.Reset();

        yield return StartCoroutine(FillEmptyTiles());
        Finished?.Invoke();
    }

    IEnumerator ClearLine(TileData[] line, Action clearingDone)
    {
        var particles = ParticlesСreate.instance.ConfigParticles(line);
        float step = Time.fixedDeltaTime * particlesMoveSpeed;

        var main = particles.main;
        main.duration = line.Length * particlesTimePerTile;

        particles.gameObject.SetActive(true);
        particles.Play(true);

        LineCleared?.Invoke(line);
        AudioManager.instance.PlayPlayersTurnEffect(SoundEffects.Cleared);
        TileCreate.instance.DisableTile(line[0]);

        bool move = true;
        int endIndex = 1;
        Vector2 endPos = line[endIndex].constPosition;

        while (move)
        {
            while ((Vector2)particles.transform.position != endPos)
            {
                particles.transform.position = Vector2.MoveTowards(particles.transform.position, endPos, step);
                yield return new WaitForFixedUpdate();
            }

            TileCreate.instance.DisableTile(line[endIndex]);

            if (endIndex < line.Length - 1)
            {
                endIndex++;
                endPos = line[endIndex].constPosition;
            }

            else move = false;

            yield return null;
        }

        particles.Stop();
        ParticlesСreate.instance.DisableParticles(particles);
        clearingDone?.Invoke();
    }

    IEnumerator DropTile(TileData startTileData, TileData targetTileData, Action dropEnded)
    {
        float step = tileDropAcceleration * Time.fixedDeltaTime;
        GameObject tileObj = startTileData.tileObj;

        while ((Vector2)tileObj.transform.position != targetTileData.constPosition)
        {
            tileObj.transform.position = Vector2.MoveTowards(tileObj.transform.position, targetTileData.constPosition, step);
            step += tileDropAcceleration * Time.fixedDeltaTime;
            yield return new WaitForFixedUpdate();
        }

        startTileData.UpdateTileObject(emptyTileObj);
        targetTileData.UpdateTileObject(tileObj);
        dropEnded?.Invoke();
    }

    IEnumerator FillEmptyTiles()
    {        
        for (int x = 0; x < TileCreate.instance.XSize; x++)
        {
            int queueNumber = 0;

            for (int y = TileCreate.instance.YSize - 1; y > 0; y--)
            {
                TileData targTileData = TileCreate.instance.boardTilesData[x, y];
                if (targTileData.tileObj == emptyTileObj)
                {
                    var tileObj = TileCreate.instance.GetRandomTileObj(x, queueNumber);
                    TileData startTileData = new TileData(tileObj, -1, -1);
                    StartCoroutine(DropTile(startTileData, targTileData, fillEmptyObserver.JobFinished));
                    fillEmptyObserver.JobAdded();
                    queueNumber++;
                }

                else break;
            }
        }

        while (!fillEmptyObserver.AllJobsFinished())
            yield return null;
    }
}
