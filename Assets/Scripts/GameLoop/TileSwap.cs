﻿using System;
using System.Collections;
using UnityEngine;

public class TileSwap : MonoBehaviour
{
    public static TileSwap instance;
    public event Action SwapStarted;
    public event Action SwapEnded;
    [HideInInspector] public bool isPlayersTurn = true;

    [SerializeField] float speed;
    TileData prevTileData = null;
    TileData callerData = null;
    int xSize;
    int ySize;

    static readonly Color selectedColor = new Color(.5f, .5f, .5f, 1.0f);

    private void Awake()
    {
        if (instance == null)
            instance = this;
        else if (instance != this)
            Destroy(gameObject.GetComponent<TileSwap>());
    }

    public void Init()
    {
        xSize = TileCreate.instance.XSize;
        ySize = TileCreate.instance.YSize;
    }

    public void HandleSelect(Tile tile)
    {
        if (isPlayersTurn)
        {
            callerData = TileCreate.instance.boardTilesData[tile.tileData.boardX, tile.tileData.boardY];

            if (prevTileData == null)
                Select();

            else if (prevTileData.Tile == tile)
                Deselect(prevTileData);

            else if (prevTileData.Renderer.sprite != callerData.Renderer.sprite)
            {
                if (CheckSwapPossibility(out Vector2 direction))
                    StartCoroutine(SwapTilesProcess());

                else Deselect(prevTileData);
            }
        }        
    }

    void Select()
    {
        prevTileData = callerData;
        callerData.Renderer.color = selectedColor;
        AudioManager.instance.PlayPlayersTurnEffect(SoundEffects.Selected);
    }

    void Deselect(TileData tileData)
    {
        prevTileData = null;
        tileData.Renderer.color = Color.white;
    }

    bool CheckSwapPossibility(out Vector2 direction)
    {
        TileData[,] boardTiles = TileCreate.instance.boardTilesData;
        int prevTile_x = prevTileData.boardX;
        int prevTile_y = prevTileData.boardY;

        if (prevTile_x > 0 && callerData == boardTiles[prevTile_x - 1, prevTile_y])
        {
            direction = Vector2.right;
            return true;
        }

        else if (prevTile_x < xSize - 1 && callerData == boardTiles[prevTile_x + 1, prevTile_y])
        {
            direction = Vector2.left;
            return true;
        }

        else if (prevTile_y > 0 && callerData == boardTiles[prevTile_x, prevTile_y - 1])
        {
            direction = Vector2.up;
            return true;
        }

        else if (prevTile_y < ySize - 1 && callerData == boardTiles[prevTile_x, prevTile_y + 1])
        {
            direction = Vector2.down;
            return true;
        }

        else
        {
            direction = Vector2.zero;
            return false;
        }
    }

    IEnumerator SwapTilesProcess()
    {
        SwapStarted.Invoke();
        AudioManager.instance.PlayPlayersTurnEffect(SoundEffects.Swapped);

        Vector2 callerTileTargPos = prevTileData.constPosition;
        Vector2 prevTileTargPos = callerData.constPosition;
        float step = speed * Time.fixedDeltaTime;

        while ((Vector2)callerData.tileObj.transform.position != callerTileTargPos
            && (Vector2)prevTileData.tileObj.transform.position != prevTileTargPos)
        {
            callerData.tileObj.transform.position = Vector2.MoveTowards(callerData.tileObj.transform.position, callerTileTargPos, step);
            prevTileData.tileObj.transform.position = Vector2.MoveTowards(prevTileData.tileObj.transform.position, prevTileTargPos, step);
            yield return new WaitForFixedUpdate();
        }

        GameObject _temp = callerData.tileObj;
        callerData.UpdateTileObject(prevTileData.tileObj);
        prevTileData.UpdateTileObject(_temp);

        Deselect(callerData);

        SwapEnded?.Invoke();
    }
}
