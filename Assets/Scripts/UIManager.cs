﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class UIManager : MonoBehaviour
{
    public static UIManager instance;

    [SerializeField] TextMeshProUGUI scoreObject;
    [SerializeField] TextMeshProUGUI comboCounterObject;
    [SerializeField] TextMeshProUGUI comboAnimationObject;
    [SerializeField] TextMeshProUGUI welcomeObject;
    [SerializeField] float welcomeScreenShowTime;
   
    int bestCombo = 0;
    Animator comboAnimator;

    private void Awake()
    {
        if (instance == null)
            instance = this;
        else if (instance != this)
            Destroy(gameObject.GetComponent<UIManager>());
    }

    private void Start()
    {
        scoreObject.text = "Score: 0";
        comboCounterObject.text = string.Empty;
        comboAnimationObject.gameObject.SetActive(false);

        comboAnimator = comboAnimationObject.GetComponent<Animator>();
        ScoreHandler.instance.UpdateScore += OnScoreUpdate;
        GameManager.instance.ComboCounterChanged += OnComboUpdate;
    }

    void OnScoreUpdate(int lineScore) => scoreObject.text = $"Score: {lineScore}";

    void OnComboUpdate(int count)
    {
        if (count > 1)
        {
            comboAnimationObject.text = $"Combo X{count}";
            comboAnimator.gameObject.SetActive(true);
            comboAnimator.SetFloat("animVariant", 0);
            float animLength = comboAnimator.GetCurrentAnimatorStateInfo(0).length;
            StartCoroutine(DisableComboAnimation(animLength));

            AudioManager.instance.PlayComboEffect();

            if (count > bestCombo)
            {
                bestCombo = count;
                comboCounterObject.text = $"Best combo: X{bestCombo}";
            }
        }        
    }

    IEnumerator DisableComboAnimation(float timer)
    {
        yield return new WaitForSeconds(timer);
        comboAnimator.gameObject.SetActive(false);
        comboAnimator.SetFloat("animVariant", -1);
    }

    public IEnumerator RunWelcomeScreen()
    {
        scoreObject.gameObject.SetActive(false);
        comboCounterObject.gameObject.SetActive(false);
        comboAnimationObject.gameObject.SetActive(false);

        welcomeObject.text = $"Welcome to yet another\nMatch3 game\n\nCurrent highscore: {PlayerPrefs.GetInt("Highscore")}\nCurrent max combo: {PlayerPrefs.GetInt("ComboRecord")}" +
            "\n\n Now relax and have fun!";

        welcomeObject.gameObject.SetActive(true);
        yield return new WaitForSeconds(welcomeScreenShowTime);

        welcomeObject.gameObject.SetActive(false);
        scoreObject.gameObject.SetActive(true);
        comboCounterObject.gameObject.SetActive(true);

        Destroy(welcomeObject.gameObject);
    }
}
