﻿using UnityEngine;

public class TileData
{
    public TileData(GameObject tileObj, int xPos, int yPos)
    {
        UpdateTileObject(tileObj);
        constPosition = tileObj.transform.position;
        boardX = xPos;
        boardY = yPos;
    }

    public readonly int boardX;
    public readonly int boardY;
    public readonly Vector2 constPosition;

    public GameObject tileObj { get; private set; }
    public Tile Tile => tileObj.GetComponent<Tile>();
    public SpriteRenderer Renderer => tileObj.GetComponent<SpriteRenderer>();

    public void UpdateTileObject(GameObject newTileObj)
    {
        tileObj = newTileObj;
        if (tileObj != null)
            tileObj.GetComponent<Tile>().tileData = this;
    }
}
