﻿using System;

public class CoroutinesObserver
{
    public CoroutinesObserver()
    {
        JobFinished = new Action(IncrementFinishedJobs);
    }

    int jobsStarted = 0;
    int jobsFinished = 0;

    public Action JobFinished;

    public void JobAdded(int jobs = 1) => jobsStarted += jobs;
    public bool AllJobsFinished() => jobsStarted == jobsFinished;

    public void Reset()
    {
        jobsStarted = 0;
        jobsFinished = 0;
    }

    void IncrementFinishedJobs() => jobsFinished++;
}