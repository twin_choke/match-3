﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu (fileName = "NewTilesSet", menuName = "TileContainersSet")]
public class TilesSet : ScriptableObject
{
    [SerializeField] private List<TileContainer> tiles = new List<TileContainer>();

    [HideInInspector] public List<TileContainer> Tiles => tiles;
}
