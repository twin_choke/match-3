﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tile : MonoBehaviour
{
    void Start()
    {
        TileSelect += TileSwap.instance.HandleSelect;
    }

    public event Action<Tile> TileSelect;

    public TileData tileData;

    void OnMouseDown() => TileSelect?.Invoke(this);
}