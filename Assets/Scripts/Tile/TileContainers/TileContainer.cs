﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "NewTileContainer", menuName = "TileContainer")]
public class TileContainer : ScriptableObject
{
    [SerializeField] Sprite sprite;
    [SerializeField] GameObject clearEffect;
    [SerializeField] int reward;

    [HideInInspector] public Sprite Sprite => sprite;
    [HideInInspector] public GameObject ClearEffect => clearEffect;
    [HideInInspector] public int Reward => reward;
}
