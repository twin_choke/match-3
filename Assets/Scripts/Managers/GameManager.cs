﻿using System;
using System.Collections;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    public static GameManager instance;
    public event Action<int> ComboCounterChanged;
    public event Action EmptyTurn;

    void Awake()
    {
        if (instance == null)
            instance = this;
        else if (instance != this)
            Destroy(gameObject.GetComponent<GameManager>());
    }

    void Start()
    {
        StartCoroutine(StartGame());
    }

    void Init()
    {
        TileCreate.instance.Init();
        TileSwap.instance.Init();
        TileLineSearch.instance.Init();
        ParticlesСreate.instance.Init();
        ScoreHandler.instance.Init();

        TileSwap.instance.SwapStarted += PlayerTurnDeactivate;
        TileSwap.instance.SwapEnded += HandleSwap;

        TileCreate.instance.GameStartBoardFill();
        LineClear.instance.Init();
    }

    IEnumerator StartGame()
    {
        yield return StartCoroutine(UIManager.instance.RunWelcomeScreen());
        Init();
    }

    void HandleSwap() => StartCoroutine(ManageLinesCheck());

    IEnumerator ManageLinesCheck()
    {
        int comboCount = 0;
        ComboCounterChanged?.Invoke(comboCount);

        while (TileLineSearch.instance.CheckBoardLines())
        {
            comboCount += TileLineSearch.instance.TileLinesList.Count;
            ComboCounterChanged?.Invoke(comboCount);
            yield return StartCoroutine(LineClear.instance.RunLinesClearing());
        }

        if (comboCount == 0)
            EmptyTurn?.Invoke();

        TileSwap.instance.isPlayersTurn = true;   
    }

    void PlayerTurnDeactivate() => TileSwap.instance.isPlayersTurn = false;
}
