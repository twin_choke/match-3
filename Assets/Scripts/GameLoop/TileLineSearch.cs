﻿using System.Collections.Generic;
using UnityEngine;

public class TileLineSearch : MonoBehaviour
{
    public static TileLineSearch instance;

    [SerializeField] int minIdenticalRow;
    TileData[,] boardTilesData;
    int xSize;
    int ySize;

    public List<TileData[]> TileLinesList { get; private set; }
    List<TileData> TileLine;

    private void Awake()
    {
        if (instance == null)
            instance = this;
        if (instance != this)
            Destroy(gameObject.GetComponent<TileLineSearch>());
    }

    public void Init()
    {
        xSize = TileCreate.instance.XSize;
        ySize = TileCreate.instance.YSize;
        boardTilesData = TileCreate.instance.boardTilesData;
        TileLinesList = new List<TileData[]>(10);
        TileLine = new List<TileData>(Mathf.Max(xSize, ySize));
    }

    public bool CheckBoardLines()
    {
        TileLinesList.Clear();
        TileLine.Clear();

        CheckColomns();
        CheckRows();

        bool anyLinesFound = TileLinesList.Count != 0;

        return anyLinesFound;
    }

    void CheckColomns()
    {
        boardTilesData = TileCreate.instance.boardTilesData;

        for (int x = 0; x < xSize; x++)
        {
            for (int y = 0; y < ySize; y++)
            {
                TileData tData = boardTilesData[x, y];
                if (TileLine.Count > 0)
                {
                    if (tData.Renderer.sprite == TileLine[TileLine.Count - 1].Renderer.sprite)
                    {
                        TileLine.Add(tData);
                        if (y == ySize - 1 && TileLine.Count >= minIdenticalRow)
                            TileLinesList.Add(TileLine.ToArray());

                    }

                    else
                    {
                        if (TileLine.Count >= minIdenticalRow)
                        {
                            TileLinesList.Add(TileLine.ToArray());
                        }

                        TileLine.Clear();
                        TileLine.Add(tData);
                    }
                }

                else TileLine.Add(tData);
            }

            TileLine.Clear();
        }

        TileLine.Clear();
    }

    void CheckRows()
    {
        boardTilesData = TileCreate.instance.boardTilesData;

        for (int y = 0; y < ySize; y++)
        {
            for (int x = 0; x < xSize; x++)
            {
                TileData tData = boardTilesData[x, y];
                if (TileLine.Count > 0)
                {
                    if (tData.Renderer.sprite == TileLine[TileLine.Count - 1].Renderer.sprite)
                    {
                        TileLine.Add(tData);
                        if (x == xSize - 1 && TileLine.Count >= minIdenticalRow)
                            TileLinesList.Add(TileLine.ToArray());
                    }


                    else
                    {
                        if (TileLine.Count >= minIdenticalRow)
                        {
                            TileLinesList.Add(TileLine.ToArray());
                        }

                        TileLine.Clear();
                        TileLine.Add(tData);
                    }
                }

                else TileLine.Add(tData);
            }

            TileLine.Clear();
        }

        TileLine.Clear();
    }
}
