﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ParticlesСreate : MonoBehaviour
{
    public static ParticlesСreate instance;

    [SerializeField] float lifetimeAfterEmitStop;
    [SerializeField] int poolLength;

    Dictionary<Sprite, List<ParticleSystem>> SpriteParticlesDict;
    List<TileContainer> TileContainers;

    void Awake()
    {
        if (instance == null)
            instance = this;
        else if (instance != this)
            Destroy(gameObject.GetComponent<ParticlesСreate>());
    }

    public void Init()
    {
        this.TileContainers = TileCreate.instance.TileSet.Tiles;
        SpriteParticlesDict = new Dictionary<Sprite, List<ParticleSystem>>(TileContainers.Count);

        foreach (var tile in TileContainers)
        {
            Sprite sprite = tile.Sprite;
            List<ParticleSystem> particlesList = new List<ParticleSystem>(poolLength);

            for (int i = 0; i < poolLength; i++)
            {
                var particles = Instantiate(tile.ClearEffect, gameObject.transform).GetComponent<ParticleSystem>();
                particles.gameObject.SetActive(false);
                particlesList.Add(particles);
            }

            SpriteParticlesDict.Add(sprite, particlesList);
        }
    }

    public ParticleSystem ConfigParticles(TileData[] line)
    {
        var particles = GetParticles(line);

        if ((line[0].constPosition.x - line[1].constPosition.x) != 0) // if line is horizontal
        {
            particles.transform.rotation = Quaternion.Euler(0, 0, 90);
        }

        else // if line is vertical
        {
            particles.transform.rotation = Quaternion.Euler(0, 0, 0);
            Array.Reverse(line, 0, line.Length);
        }

        particles.transform.position = line[0].constPosition;
        return particles;
    }

    ParticleSystem GetParticles(TileData[] line)
    {
        ParticleSystem particles = null;
        Sprite sprite = null;

        for (int j = 0; j < line.Length; j++)
        {
            if (line[j].Renderer.sprite != null)
            {
                sprite = line[j].Renderer.sprite;
                break;
            }
        }

        var particleList = SpriteParticlesDict[sprite];

        for (int i = 0; i < particleList.Count; i++)
        {
            if (!particleList[i].gameObject.activeSelf)
            {
                particles = particleList[i];
                break;
            }
        }

        if (particles == null) // if no free particles found in pool
        {
            Debug.LogWarning("Not enough particles in pool!");
            foreach (var tile in TileContainers)
            {
                if (tile.Sprite == sprite)
                {
                    particles = Instantiate(tile.ClearEffect, gameObject.transform).GetComponent<ParticleSystem>();
                    particles.gameObject.SetActive(false);
                    SpriteParticlesDict[sprite].Add(particles);
                }
            }                
        }

        return particles;
    }

    public void DisableParticles(ParticleSystem particles) => StartCoroutine(RunParticlesDisabling(particles));

    IEnumerator RunParticlesDisabling(ParticleSystem particles)
    {
        yield return new WaitForSeconds(lifetimeAfterEmitStop);
        particles.gameObject.SetActive(false);
    }
}