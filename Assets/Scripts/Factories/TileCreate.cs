﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class TileCreate : MonoBehaviour
{
    public static TileCreate instance;
    [HideInInspector] public bool isFilling;

    [SerializeField] GameObject tileObjPref;
    [SerializeField] GameObject tilesSpawnObj;
    [SerializeField] TilesSet tilesSet;
    [SerializeField] int xSize;
    [SerializeField] int ySize;
    [SerializeField] float tileExtraOffset;
    [SerializeField] int tilePoolLength;

    [HideInInspector] public GameObject emptyTileObj;
    public TileData[,] boardTilesData;
    public TilesSet TileSet => tilesSet;
    public int XSize => xSize;
    public int YSize => ySize;

    Queue<GameObject> tileObjectsPool;
    List<Sprite> possibleSprites;
    float xOffset;
    float yOffset;
    Vector2 spawnObjPosition;

    void Awake()
    {
        if (instance == null)
            instance = this;
        else if (instance != this)
            Destroy(gameObject.GetComponent<TileCreate>());
    }

    public void Init()
    {
        tileObjectsPool = new Queue<GameObject>(xSize * ySize);
        possibleSprites = new List<Sprite>(tilesSet.Tiles.Count);

        spawnObjPosition = tilesSpawnObj.transform.position;
        Vector2 spriteSize = tileObjPref.GetComponent<SpriteRenderer>().bounds.size;
        xOffset = spriteSize.x + tileExtraOffset;
        yOffset = spriteSize.y + tileExtraOffset;
    }

    public void GameStartBoardFill()
    {
        Vector3 boardPos = transform.position;
        boardTilesData = new TileData[xSize, ySize];
        possibleSprites.Clear();

        for (int y = 0; y < ySize; y++)
        {
            for (int x = 0; x < xSize; x++)
            {
                possibleSprites.AddRange(tilesSet.Tiles.Select(t => t.Sprite));

                if (x > 0)
                    possibleSprites.Remove(boardTilesData[x - 1, y].Renderer?.sprite);
                if (y > 0)
                    possibleSprites.Remove(boardTilesData[x, y - 1].Renderer?.sprite);

                var newTile = Instantiate(tileObjPref, new Vector2(boardPos.x + x * xOffset, boardPos.y + y * yOffset), tileObjPref.transform.rotation, gameObject.transform);
                var tileData = new TileData(newTile, x, y);
                boardTilesData[x, y] = tileData;

                var newSprite = possibleSprites[Random.Range(0, possibleSprites.Count)];
                newTile.GetComponent<SpriteRenderer>().sprite = newSprite;

                possibleSprites.Clear();
            }
        }

        emptyTileObj = Instantiate(tileObjPref, gameObject.transform);
        emptyTileObj.GetComponent<SpriteRenderer>().sprite = null;
    }

    public GameObject GetRandomTileObj(int column, int queueNumber)
    {
        Vector2 spawnPos = new Vector2(boardTilesData[column, 0].constPosition.x, spawnObjPosition.y + queueNumber * yOffset);

        var tileObj = tileObjectsPool.Dequeue();
        tileObj.SetActive(true);
        tileObj.transform.position = spawnPos;

        possibleSprites.Clear();
        possibleSprites.AddRange(tilesSet.Tiles.Select(t => t.Sprite));

        tileObj.GetComponent<SpriteRenderer>().sprite = possibleSprites[Random.Range(0, possibleSprites.Count)];

        return tileObj;
    }   

    public void DisableTile(TileData tileData)
    {       
        if (tileData.tileObj != emptyTileObj)
        {
            tileData.tileObj.SetActive(false);
            tileObjectsPool.Enqueue(tileData.tileObj);
            tileData.UpdateTileObject(emptyTileObj);
        }
    }
}